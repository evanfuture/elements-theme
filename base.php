<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

  <?php wp_head(); ?>

  <!-- FOR ANGULAR ROUTING -->
  <base href="/">

</head>

<body <?php body_class(); ?> ng-app="elementsApp" ng-controller="appController as app">




<!-- ================== HEADER ======================-->
<header class="banner">
  <div class="container">
    <a class="brand" href="#home">
      <img
      src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/logo-landscape.png"
      srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/logo-landscape.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/logo-landscape@2x.png 2x"
      alt="Elements Interactive"
      >
    </a>
    <nav class="nav-primary">
      <ul>
        <li><a href="#home">Home</a></li>
        <li><a href="#careers">Careers</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#blog">Blog</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </nav>
  </div>
</header>




<!-- ================== CAROUSEL ======================-->
<div class="swiper-container carousel" id="carousel">
  <div class="swiper-wrapper">

    <div class="swiper-slide" ng-show="app.carousel.processing" style="background-color: #ffffff;">
      <div class="carousel-content">
        <p class="carousel-title">Loading...</p>
      </div>
    </div>

    <div class="swiper-slide" ng-repeat="slide in app.carousel.results" style="background-image: url( {{slide.image}} );">
      <div class="carousel-content">
        <p class="carousel-title">{{slide.title}}</p>
        <p class="carousel-subtitle">{{slide.subtitle}}</p>
        <a href="{{slide.action_url}}" class="slide-action btn">{{slide.action}}</a>
      </div>
    </div>

  </div>
  <div class="swiper-pagination swiper-pagination-white swiper-pagination-clickable"></div>
  <div class="swiper-button-next swiper-button-white"></div>
  <div class="swiper-button-prev swiper-button-white"></div>
</div>




<div class="wrap container" role="document"><div class="content row"><main class="main">

  <!-- ================== TAGLINE ======================-->
  <div class="section tagline">
    <p class="lead">Creating rock solid applications for web and mobile</p>
  </div>




  <!-- ================== ABOUT ======================-->
  <div class="section about" id="about">
    <h2>Great projects for relevant companies</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi ipsam maiores obcaecati cumque eius porro vero veniam nemo atque! Exercitationem esse, amet et beatae magni nesciunt fugit vel voluptatibus non!</p>

    <div class="company-list">
      <div class="company" ng-repeat="company in app.companies.results">
        <a href="{{company.website}}" rel="_blank">
          <span class="logo">
            <img
            src="{{company.logo}}"
            srcset="{{company.logo}} 1x, {{company.logo_2x}} 2x"
            alt="{{company.name}}"
            >
          </span>
          <p class="client">{{company.name}}</p>
        </a>
      </div>
    </div>

    <p>Lorem ipsum dolor sit amet, elit. Dolorem, id, animi. Facilis voluptatibus fugiat dolore accusantium magnam, deserunt dolor, atque quod autem, laudantium ea doloribus <a href="#home">sisliet</a> omnis nam. Voluptate accusantium, <a href="#contact">contact</a>.</p>
  </div>




  <!-- ================== CAREERS ======================-->
  <div id="careers" class="section careers">
    <h2>Join a magnificent and fun team</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias numquam ea ullam facere repudiandae eum, placeat commodi quae vitae ratione iusto reiciendis blanditiis consequatur sint, suscipit animi ipsam voluptatem id?</p>
    <div class="office barcelona">
      <img
      src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/bcn-city.png"
      srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/bcn-city.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/bcn-city@2x.png 2x"
      alt="Barcelona, ES"
      >
      <div class="description">
        <img
        src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/bcn-logo.png"
        srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/bcn-logo.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/bcn-logo@2x.png 2x"
        alt="Barcelona, ES"
        >
        <h3>Barcelona, ES</h3>
        <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean commodo ligula eget dolor. Aenean massa.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean commodo ligula eget dolor. Aenean massa.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean commodo ligula eget dolor. Aenean massa.</p></div>
      </div>
    </div>
    <div class="office almere">
      <img
      src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/almere-city.png"
      srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/almere-city.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/almere-city@2x.png 2x"
      alt="Almere, NL"
      >
      <div class="description">
        <img
        src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/almere-logo.png"
        srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/almere-logo.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/about/almere-logo@2x.png 2x"
        alt="Almere, NL"
        >
        <h3>Almere, NL</h3>
        <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean commodo ligula eget dolor. Aenean massa.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean commodo ligula eget dolor. Aenean massa.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean commodo ligula eget dolor. Aenean massa.</p></div>
      </div>
    </div>
  </div>




  <!-- ================== BLOG ======================-->
  <div id="blog" class="section blog">
    <h2>Blog: Great apps and webs for all!</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, laboriosam optio ipsa error fuga. Rerum error, numquam consectetur, ducimus ab quidem maiores provident tempore consequuntur ipsum impedit reiciendis aperiam perspiciatis.</p>
    <!--====== Article ==========-->
    <div class="article">
    <!--====== Article Header ==========-->
      <div class="header">
        <div class="featured-image">
          <img
          src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/device-lab-testing.jpg"
          srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/device-lab-testing.jpg 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/device-lab-testing@2x.jpg 2x"
          alt="Device Lab Testing"
          >
        </div>
        <h1>Praesent ipsum mauris volutpat mollis faucibus.</h1>
        <p>Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. </p>
      </div>
      <!--====== Article Content ==========-->
      <div class="content">
        <div class="text">
          <p>Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien.</p>
          <p>Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu.</p>
        </div>
        <!--== Parallax==-->
        <div class="parallax-bg" ng-controller="appParallax">
          <div du-parallax y="ios" class="foreground polygon ios">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/IOS-logo.png"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/IOS-logo.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/IOS-logo@2x.png 2x"
            alt="IOS Development"
            >
          </div>
          <div du-parallax y="android" class="foreground polygon android">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/android-logo.png"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/android-logo.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/android-logo@2x.png 2x"
            alt="Android Development"
            >
          </div>
          <div du-parallax y="windows" class="middleground polygon windows">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/windows-logo.png"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/windows-logo.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/windows-logo@2x.png 2x"
            alt="Windows Phone Development"
            >
          </div>
          <div du-parallax y="nokia" class="middleground device nokia">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/nokia-lumia-device.png"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/nokia-lumia-device.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/nokia-lumia-device@2x.png 2x"
            alt="Mobile Development"
            >
          </div>
          <div du-parallax y="iphone" class="background device iphone">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/iphone-device.png"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/iphone-device.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/parallax/iphone-device@2x.png 2x"
            alt="iPhone Development"
            >
          </div>
        </div>
        <!--== Images ==-->
        <div class="images">
          <div class="image">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/device-lab-testing.jpg"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/device-lab-testing.jpg 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/device-lab-testing@2x.jpg 2x"
            alt="Device Lab Testing"
            >
          </div>
          <div class="image">
            <img
            src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/developers-analyzing.jpg"
            srcset="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/developers-analyzing.jpg 1x, <?php echo get_stylesheet_directory_uri(); ?>/dist/images/blog/developers-analyzing@2x.jpg 2x"
            alt="Device Lab Testing"
            >
          </div>
        </div>
      </div>

      <!--====== Article Comments ==========-->
      <div class="comments">
        <h2>Leave a comment</h2>
        <form ng-submit="app.saveComment()">

          <div class="fieldgroup">
            <div class="field name">
              <label class="comment-label required" for="comment-name">Name</label><span class="comment-name"><input type="text" name="comment-name" value="" size="40" class="name" aria-required="true" aria-invalid="false" placeholder="Place your name here..." ng-model="app.commentData.name"></span>
            </div>

            <div class="field email">
              <label class="comment-label required" for="comment-email">E-mail</label><span class="comment-email"><input type="text" name="comment-email" value="" size="40" class="email" aria-required="true" aria-invalid="false" placeholder="username@company.com" ng-model="app.commentData.email"></span>
            </div>
          </div>

          <div class="fieldgroup">
            <div class="field comment">
              <label class="comment-label required" for="comment-textarea">Comment</label><span class="comment-comment"><textarea name="comment-comment" cols="40" rows="10" class="comment" aria-required="true" aria-invalid="false" placeholder="Place your comment here..." ng-model="app.commentData.comment"></textarea></span>
            </div>
          </div>
          <div class="submit-button">
            <input type="submit" value="Submit comment" class="btn btn-primary">
          </div>
        </form>

        <div class="conversation">
          <div class="comment" ng-repeat="comment in app.comments.results">
            <p class="meta"><a href="#">{{comment.name}}</a><span class="date">February 23, 2015</span></p>
            <div class="text">{{comment.comment}}</div>
          </div>
        </div>

      </div>


    </div>
  </div>




  <!-- ================== CONTACT ======================-->
  <div id="contact" class="section contact">
    <h2>Let's talk and go on!</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum consectetur officiis eius veritatis ex itaque libero corrupti consequatur expedita architecto minus molestias minima corporis ipsum, animi incidunt, quis beatae, iusto.</p>

    <form ng-submit="app.saveMessage()">
      <h3>Send us a message</h2>
        <p class="required">Required Fields</p>

        <div class="fieldgroup">
          <div class="field name">
            <label class="message-label required" for="message-name">Name</label><span class="message-name"><input type="text" name="message-name" value="" size="40" class="name" aria-required="true" aria-invalid="false" placeholder="Place your name here..." ng-model="app.messageData.name"></span>
          </div>
        </div>
        <div class="fieldgroup">
          <div class="field email">
            <label class="message-label required" for="message-email">E-mail</label><span class="message-email"><input type="text" name="message-email" value="" size="40" class="email" aria-required="true" aria-invalid="false" placeholder="username@company.com" ng-model="app.messageData.email"></span>
          </div>

        </div>
        <div class="fieldgroup">
          <div class="field telephone">
            <label class="message-label" for="message-telephone">Telephone</label><span class="message-telephone"><input type="text" name="message-telephone" value="" size="40" class="name" aria-required="false" aria-invalid="false" placeholder="Place your telephone number here..." ng-model="app.messageData.telephone"></span>
          </div>
        </div>

        <div class="fieldgroup">
          <div class="field message">
            <label class="message-label required" for="message-textarea">Message</label><span class="message-message"><textarea name="message-message" cols="40" rows="10" class="message" aria-required="true" aria-invalid="false" placeholder="Place your message here..." ng-model="app.messageData.message"></textarea></span>
          </div>
        </div>
        <div class="submit-button">
          <input type="submit" value="Send" class="btn">
        </div>
      </form>
    </div>






  </main></div></div>




<!-- ================== FOOTER ======================-->
<footer class="content-info">
  <div class="container">
    <a class="copyright" href="<?= esc_url(home_url('/')); ?>">&copy; Elements Interactive</a>
    <nav class="nav-footer">
      <ul>
        <li><a href="#home">Home</a></li>
        <li><a href="#careers">Careers</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#blog">Blog</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </nav>
  </div>
</footer>

  <?php wp_footer(); ?>
  </body>
</html>
