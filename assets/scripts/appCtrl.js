angular.module('appCtrl', ['appServices'])

.controller('appController', function(Carousel, Company, Comment, Message) {

  var vm = this;

  vm.processing = true;

 /// grab all the carousel slides at page load
  Carousel.all()
    .success(function(data) {
      // bind the companies that come back to vm.carousel
      vm.carousel = data;
              // Add slider effect to the element .carousel

        vm.processing = false;
    });

    /// grab all the companies at page load
  Company.all()
    .success(function(data) {
      // bind the companies that come back to vm.companies
      vm.companies = data;
    });

     /// grab all the blog comments at page load
  Comment.all()
    .success(function(data) {
      // bind the companies that come back to vm.comments
      vm.comments = data;
    });

    // function to create a comment
  vm.saveComment = function() {

    Comment.create(vm.commentData)
      .success(function(data) {
        vm.commentData = {};
        vm.message = data.message;
      });

  };

  // function to create a message
  vm.saveMessage = function() {

    Message.create(vm.messageData)
      .success(function(data) {
        vm.messageData = {};
        vm.message = data.message;
      });

  };

})
.controller('appParallax', function($scope, parallaxHelper){
    $scope.ios = parallaxHelper.createAnimator(-0.7);
    $scope.android = parallaxHelper.createAnimator(-0.6);
    $scope.windows = parallaxHelper.createAnimator(-0.5);
    $scope.nokia = parallaxHelper.createAnimator(-0.4);
    $scope.iphone = parallaxHelper.createAnimator(-0.3);
  })
;
