angular.module('elementsApp', [
  'ngRoute',
  'appCtrl',
  'appServices',
  'duParallax'
])

// application configuration to integrate token into requests
.config(function($httpProvider, $routeProvider, $locationProvider) {

  // attach our auth interceptor to the http requests
  $httpProvider.interceptors.push('AuthInterceptor');

  $routeProvider

    // route for the main page
    .when('/', {
      templateUrl: myLocalized.base + 'base.php'
    });

  $locationProvider.html5Mode(true);

});
