angular.module('appServices', [])


// ===================================================
// application configuration to integrate token into requests
// ===================================================
.factory('AuthInterceptor', function() {

  var interceptorFactory = {};

  // this will happen on all HTTP requests
  interceptorFactory.request = function(config) {

      config.headers['Authorization'] = 'Token 076aea6ec77e1461a4ea87af6f104daecc4d1129';


    return config;
  };

  return interceptorFactory;

})

// ===================================================
// Get Carousel Slides from Rest api
// ===================================================

.factory('Carousel', function($http) {

  // create a new object
  var carouselFactory = {};

  // get all slides
  carouselFactory.all = function() {
    return $http.get('https://recruitment.elements.nl:8080/v1/carousel/');
  };

  // return our entire carouselFactory object
  return carouselFactory;

})

// ===================================================
// Get Companies from Rest api
// ===================================================

.factory('Company', function($http) {

  // create a new object
  var companyFactory = {};

  // get all companies
  companyFactory.all = function() {
    return $http.get('https://recruitment.elements.nl:8080/v1/company/');
  };

  // return our entire companyFactory object
  return companyFactory;

})

// ===================================================
// Get from and Put Comments to Rest api
// ===================================================

.factory('Comment', function($http) {

  // create a new object
  var commentFactory = {};

  // get all comments
  commentFactory.all = function() {
    return $http.get('https://recruitment.elements.nl:8080/v1/comments/');
  };

    commentFactory.create = function(commentData) {
    return $http.post('https://recruitment.elements.nl:8080/v1/comments/', commentData);
  };

  // return our entire commentFactory object
  return commentFactory;

})

// ===================================================
// Put Message to Rest api
// ===================================================

.factory('Message', function($http) {

  // create a new object
  var messageFactory = {};

  messageFactory.create = function(messageData) {
    return $http.post('https://recruitment.elements.nl:8080/v1/message/', messageData);
  };

  // return our entire messageFactory object
  return messageFactory;

});
