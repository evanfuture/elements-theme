<div class="company-list">
              <div class="company" id="{{company.id}}" ng-repeat="company in app.companies">
                <a href="{{company.website}}" rel="_blank">
                  <span class="logo">
                    <img
                      src="{{company.logo}}"
                      srcset="{{company.logo}} 1x, {{company.logo_2x}} 2x"
                      alt="{{company.name}}"
                    >
                  </span>
                  <p class="client">{{company.name}}</p>
                </a>
              </div>
              <div class="company"><a href=""><span class="logo"><img srcset="" src="" alt=""></span><p class="client"></p></a></div>
          </div>
